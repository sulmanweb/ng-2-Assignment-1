import { Component } from '@angular/core';

@Component({
    selector: 'app-success-alert',
    template: `
        <div class="alert alert-success">
            <strong>Success!</strong> You just updated!
        </div>
    `
})

export class SuccessAlertComponent {

}